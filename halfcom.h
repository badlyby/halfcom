#ifndef __HALFCOM_H__
#define __HALFCOM_H__
	#define HALFCOM_BUFFERSIZE	(256+4)
	#define HALFCOM_BEGIN		0x10
	#define HALFCOM_HIGH		0x20
	#define HALFCOM_LOW			0x30
	#define HALFCOM_END			0x40
	#define HALFCOM_DATAOK		0x50
	#define HALFCOM_DATAERROR	0x60

	extern int HALFCOM_Status;
	void HC_getData(int fuart, void(*callback)(unsigned char *,int));
	void HC_sendData(int fuart, unsigned char *data, unsigned int length);
#endif
