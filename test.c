#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include "usart.h"
#include "halfcom.h"

void cb(unsigned char *data, int length)
{
	int i;
	for(i=0;i<length;i++)
		printf("%02X ",data[i]);
	printf("\n");
}

int main(int argc, char *argv[])
{
	int fuart = -1;
	if(argc == 2)
	{
		fuart = set_usart(argv[1],115200,8,1,'n');
		if(-1 != fuart)
		{
			HC_sendData(fuart,"\x01",1);
			usleep(500*1000);
			HC_getData(fuart,cb);
			usart_close(fuart);
		}
	}
}
